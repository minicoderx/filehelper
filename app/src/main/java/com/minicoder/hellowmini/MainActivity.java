package com.minicoder.hellowmini;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.minicoder.filehelper.CharsetUtils;
import com.minicoder.filehelper.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class MainActivity extends AppCompatActivity {

    private TextView mContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContent = findViewById(R.id.tv_content);
        init();
    }

    private void init() {
        File dir = getFilesDir();
        File gbkFile = new File(dir, "gbk.txt");
        File utf8File = new File(dir, "utf8.txt");
        BufferedWriter gbk = null;
        BufferedWriter utf8 = null;
        try {
            gbk = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(gbkFile), "gbk"));
            gbk.write("中文");
            gbk.flush();
            utf8 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(utf8File), "utf8"));
            utf8.write("中文");
            utf8.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            FileUtils.close(gbk);
            FileUtils.close(utf8);
        }
        StringBuilder sb = new StringBuilder();
        Charset one = CharsetUtils.getFileCharset(gbkFile);
        Charset two = CharsetUtils.getFileCharset(utf8File);
        sb.append(gbkFile.getName() + "is" + one).append("\n");
        sb.append(utf8File.getName() + "is" + two).append("\n");
        mContent.setText(sb.toString());


    }
}