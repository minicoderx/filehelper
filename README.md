# FileHelper

#### 介绍
文档帮助工具类，包含各种对文档的操作功能
1. 包含文件编码识别工具类'CharsetUtils'


#### 安装教程
```
 repositories {
        jcenter()
        maven { url "https://jitpack.io" }
   }
   dependencies {
         implementation 'com.minicoder.utils:file-utils:1.0.4'
   }
```

#### 使用说明

1.  `CharsetUtils`可以识别当前文件的不同编码，默认了中文，俄语，韩语，日语的编码，如果不够可以自行添加
2.  `CharsetUtils.addCharsets`方法可以添加默认的字符编码，只需要添加字符编码`String`集合即可。但如果当前设备不支持，会弹出not support异常

