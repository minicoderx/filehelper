package com.minicoder.filehelper;

import java.io.Closeable;
import java.io.IOException;

public class FileUtils {
    public static void close(Closeable close){
        if (close!=null){
            try {
                close.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
