package com.minicoder.filehelper;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

public class CharsetUtils {
    /**
     * 默认用一些常用的就行了，注意一些特别的编码，虽然不会报错但转化后的内容也是火星文，一般适用于不同语言之间的识别
     */
    private static final HashMap<String, Charset> DEFAULT = new LinkedHashMap<>();

    private static final String TAG = CharsetUtils.class.getSimpleName();

    private static final String[] CHARSET_NAMES = {
            "UTF-8",
            // 简体中文，注意此处不能用GBK
            "GB2312",
            "US-ASCII",
            // 俄语
            "windows-1251",
            // 日语
            "Shift_JIS",
            // 韩语
            "x-windows-949"
    };

    static {
        for (String name : CHARSET_NAMES) {
            DEFAULT.put(name, Charset.forName(name));
        }
    }

    /**
     * 添加需要增加的
     */
    public static void addCharsets(Set<String> charsets) throws Exception {
        if (!charsets.isEmpty()) {
            for (String name : charsets) {
                if (!Charset.isSupported(name)) {
                    throw new Exception(name + " is not support");
                }
                DEFAULT.put(name, Charset.forName(name));
            }
        }
    }


    /**
     * 获取文档的编码格式
     * 1.遍历设置的编码格式
     * 2.若转码异常，则排除当前编码，新的编码从之前异常的部分重新开始检验
     *
     * @param file
     * @return
     */
    public static Charset getFileCharset(File file) {
        BufferedReader reader = null;
        long skip = 0;
        String line;
        Charset curCharset = null;
        for (Charset charset : DEFAULT.values()) {
            if (!Charset.isSupported(charset.name())) {
                continue;
            }
            curCharset = charset;
            CharsetDecoder decoder = charset.newDecoder();
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), decoder));
                reader.skip(skip);
                while ((line = reader.readLine()) != null) {
                    skip += line.getBytes().length;
                }
                return charset;
            } catch (Exception e) {
                Log.e(TAG, "Error", e);
            } finally {
                FileUtils.close(reader);
            }
        }
        return curCharset;
    }
}
